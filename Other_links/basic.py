from ctypes import sizeof
import pandas as pd
from IPython.display import display
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.lines import Line2D
""" Fig init """ 
plt.style.use("/Users/sbhardwa/OneDrive - University of Edinburgh/phd_files/Year3/style.mplstyle") #matplotlib style sheet 
fig1 = plt.figure(figsize=(8,7))
# fig1 = plt.figure(figsize=(6,5.5))
ax1 = plt.axes()

SIZES=[0.134217728,
4.294967296,
8.589934592,
17.17986918,
34.35973837,
68.71947674]

AVG_BENCHIO_MPI=[1.078726055,
1.331341995,
1.290447531,
1.390062251,
3.161564643,
6.373895045]

AVG_BENCHIO_HDF5=[0.855160914,
0.855628229,
0.884348027,
0.97593415,
1.598057786,
2.836264212] 

AVG_BENCHMARK_MPI=[1.7,
1.55,
1.48,
1.5,
3.08,
6.04]

AVG_BENCHMARK_HDF5=[0.923333333,
1.106666667,
1.106666667,
1.174333333,
1.512,
2.55]


ax1.plot(SIZES,AVG_BENCHIO_HDF5, label="HDF5_benchio", color='b', linestyle='dashed')
ax1.plot(SIZES,AVG_BENCHMARK_HDF5, label="HDF5_benchmark",color='b', linestyle='solid')
ax1.plot(SIZES,AVG_BENCHIO_MPI, label="MPI_benchio", color='k',linestyle='dashed')
ax1.plot(SIZES,AVG_BENCHMARK_MPI, label="MPI_benchmark", color='k', linestyle='solid')

ax1.legend()
ax1.set_xlabel("Global Size(GB)")
ax1.set_ylabel("Averate Rate (GB/s)")
fig1.suptitle("Weak scaling comparison max striping")
fig1.tight_layout()
# plt.grid()
plt.show()
# plt.savefig("benchiobenchmark.pdf")